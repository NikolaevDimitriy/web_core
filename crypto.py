__author__ = 'dimitriy'

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicNumbers
import binascii
import jwt.utils

encoding = 'utf-8'


def create_rsa_public_key(n, e):
    n = int(binascii.hexlify(jwt.utils.base64url_decode(bytes(n, encoding))), 16)
    e = int(binascii.hexlify(jwt.utils.base64url_decode(bytes(e, encoding))), 16)
    return RSAPublicNumbers(e, n).public_key(default_backend())