__author__ = 'dimitriy'
from aiohttp_session import SESSION_KEY, get_session
from web_core.views import mixins as mx
from web_core.views import TemplateView


class ProcessFormView(mx.FormMixin, TemplateView):

    """
    A mixin that renders a form on GET and processes it on POST.
    """
    async def get_request_session(self):
        session = self.request.get(SESSION_KEY, None)
        if session is None:
            session = await get_session(self.request)
        return session

    async def get(self, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """

        return self.render_to_response(self.request, await self.get_context_data(
                form=self.get_form(self.request.app, await self.get_request_session())
                , **kwargs))

    async def post(self, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form(self.request.app, await self.get_request_session(), await self.request.post())
        is_valid = await form.validate()
        if is_valid:
            return await self.form_valid(form, **kwargs)
        else:
            return await self.form_invalid(form, **kwargs)