__author__ = 'dimitriy'
import abc

import aiohttp_jinja2
from aiohttp.web import Response, HTTPFound
from web_core.exceptions import ImproperlyConfigured
from web_core.json_helper import serialize, deserialize
from web_core.defaults import CSRF_TOKEN_NAME


class ContextMixin(object):
    """
    A default context mixin that passes the keyword arguments received by
    get_context_data as the template context.
    """
    extra_context = {}

    async def get_context_data(self, **kwargs):
        if 'view' not in kwargs:
            kwargs['view'] = self
        for key, get_data in self.extra_context.items():
            kwargs[key] = get_data() if hasattr(get_data, '__call__') else get_data
        return kwargs


class ApplicationBasePageMixin(object):
    page_identity = None
    page_title = None

    def get_page_identity(self):
        if self.page_identity:
            return self.page_identity
        raise ImproperlyConfigured("No page identity")

    def get_page_title(self):
        if self.page_title:
            return self.page_title
        raise ImproperlyConfigured("No page title")


class TemplateResponseMixin(object):
    """
    A mixin that can be used to render a template.
    """
    template_name = None
    content_type = None

    def render_to_response(self, request, context, **kwargs):
        response = aiohttp_jinja2.render_template(self.get_template_name(), request, context)
        if request.app.debug and 'form' in context:
            response.set_cookie(CSRF_TOKEN_NAME, context['form'].csrf_token.current_token)
        return response

    def get_template_name(self):
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateResponseMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_names()'")
        else:
            return self.template_name


class JsonResponseMixin(object):
    converter = None

    def render_to_response_json(self, context):
        return Response(text=self.convert_context_to_json(context), content_type='application/json')

    def convert_context_to_json(self, context, **kwargs):
        return serialize(context)


class MixedResponseMixin(TemplateResponseMixin, JsonResponseMixin):
    content_type_name = 'content_type'

    def render_to_response(self, request, context, **kwargs):
        if kwargs.get(self.content_type_name, 'template') == 'json':
            return self.render_to_response_json(context)
        return super(MixedResponseMixin, self).render_to_response(request, context)


class SingleObjectMixin(ContextMixin):

    model = None
    storage = None
    context_object_name = None
    pk_url_kwarg = 'pk'
    provide_default = False  # return default object if no pk else raise exception

    async def get_object(self, pk=0, data_providers=None):
        if pk == 0:
            return self.get_default_object()
        if data_providers is None or (data_providers.pg_db is None and data_providers.redis is None and data_providers.mongo is None):
            raise ImproperlyConfigured("SingleObjectMixin requires pg_db or redis or mongo engine")

        if self.storage is not None:
            st = self.storage(data_providers=data_providers)
            obj = await st.get(pk)
            return obj
        raise ImproperlyConfigured("SingleEditorMixin requires either a definition of 'storage'")

    def get_default_object(self):
        if self.provide_default:
            return self.model.get_default()
        raise AttributeError("Generic detail view %s doesn't provide default object" % self.__class__.__name__)

    async def get_context_data(self, **kwargs):
        """
        Insert the single object into the context dict.
        """
        context = {}
        if self.object:
            context[self.get_context_object_name()] = self.object
        context.update(kwargs)
        context = await super(SingleObjectMixin, self).get_context_data(**context)
        return context

    def get_context_object_name(self):
        if self.context_object_name:
            return self.context_object_name
        return 'object'


class MultipleObjectMixin(ContextMixin):
    model = None
    storage = None
    paginate_by = None
    page_kwarg = 'page'
    context_object_name = None
    ordering = None

    async def get_object_list(self, data_providers=None, **kwargs):

        if data_providers is None or (data_providers.pg_db is None and data_providers.redis is None and data_providers.mongo is None):
            raise ImproperlyConfigured("MultipleObjectMixin requires pg_db or redis or mongo engine")
        if self.storage is not None:
            page = int(self.request.GET.get(self.page_kwarg, 0))
            st = self.storage(data_providers=data_providers)
            obj_list = await  st.get_list(order_by=self.get_ordering(),
                                          page=page, limit=self.get_paginate_by(),
                                          conditions=self.get_conditions(), **kwargs)
            return obj_list
        else:
            pass
        return []

    def get_ordering(self):
        """
        Return the field or fields to use for ordering the queryset.
        """
        return self.ordering

    def get_paginate_by(self):
        """
        Get the number of items to paginate by, or ``None`` for no pagination.
        """
        return self.paginate_by

    def get_conditions(self):
        return []

    def get_context_object_name(self):
        """
        Get the name of the item to be used in the context.
        """
        if self.context_object_name:
            return self.context_object_name
        elif self.model:
            return '%s_list' % self.model.__name__
        else:
            return 'object_list'

    async def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = {}
        if self.object_list:
            context[self.get_context_object_name()] = self.object_list
        context.update(kwargs)
        context = await super(MultipleObjectMixin, self).get_context_data(**context)
        return context


class FormMixin(ContextMixin):
    form_class = None
    success_url = None
    template_form_var_name = 'form'
    model = None

    def get_form(self, application, csrf_context, data=None):
        if self.form_class is not None and callable(self.form_class):
            return self.form_class(application, formdata=data, meta={'csrf_context': csrf_context})
        raise ImproperlyConfigured("ProcessFormView requires either a definition of "
                                   "'form_class' or an implementation of 'get_form()'")

    async def form_valid(self, form, **kwargs):
        """
        If the form is valid, redirect to the supplied URL.
        """
        return HTTPFound(self.get_success_url(form))

    async def form_invalid(self, form, **kwargs):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        return self.render_to_response(self.request, await self.get_context_data(form, **kwargs))

    def get_success_url(self, form):
        next_url = form.data.get('next', None)
        if next_url:
            return next_url
        if self.success_url is not None:
            return self.success_url
        raise ImproperlyConfigured("ProcessFormView requires either a definition of "
                                   "'success_url' or an implementation of 'get_success_url()'")

    async def get_context_data(self, form, **kwargs):
        context = await super(FormMixin, self).get_context_data(**kwargs)
        context[self.template_form_var_name] = form
        return context

    def create_object(self):
        if self.model is not None and callable(self.model):
            return self.model()
        raise ImproperlyConfigured("FormMixin requires either a definition of "
                                   "'model' or an implementation of 'create_object()'")


class ActionMixinBase(metaclass=abc.ABCMeta):
    controller = None
    action_param_name = 'action'
    is_rpc_view = False

    def get_controller(self):
        if self.controller:
            return self.controller
        return None

    async def handle_action(self, request, *arg, **kwargs):
        """handle controller method"""
        await self.read_request_body(request)
        action = self.get_action(request)
        # controller method must return dict of params to json serialize
        controller_instance = await self.init_controller(request)
        if hasattr(controller_instance, action):
            context = await getattr(controller_instance, action)()
            return context
        raise ImproperlyConfigured("Controller '%s' requires either a definition of '%s' action" %
                                   (controller_instance.__class__.__name__, action,))

    def get_action(self, request):
        """get action name from request, by default return update method name"""
        if self.is_rpc_view:
            return self.get_rpc_action(request)
        return request.match_info.get(self.action_param_name, 'update').lower()

    async def init_controller(self, request):
        t_controller = self.get_controller()
        if t_controller is not None:
            request_kwargs = await self.get_request_params(request)
            return t_controller(request.user, request.app.data_providers, **request_kwargs)
        raise ImproperlyConfigured("ActionMixin requires either a definition of 'controller'")

    @abc.abstractmethod
    async def read_request_body(self, request):
        pass

    @abc.abstractmethod
    async def get_rpc_action(self, request):
        return 'update'

    @abc.abstractmethod
    async def get_request_params(self, request):
        return {'query_string': request.q}


class ActionPostMixin(ActionMixinBase):

    async def read_request_body(self, request):
        await request.post()

    async def get_rpc_action(self, request):
        request.POST.get(self.action_param_name, 'update').lower()

    async def get_request_params(self, request):
        return {'data': request.POST}


class ActionJsonMixin(ActionMixinBase):

    async def read_request_body(self, request):
        await request.json(loads=deserialize)

    async def get_rpc_action(self, request):
        json = await request.json()
        json.get(self.action_param_name, 'update').lower()

    async def get_request_params(self, request):
        return {'data': await request.json()}
