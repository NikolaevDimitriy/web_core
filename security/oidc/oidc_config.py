__author__ = 'dimitriy'

from enum import Enum
from urllib.parse import urljoin, urlencode
from aiohttp import BasicAuth, ClientSession
from web_core.json_helper import deserialize
from web_core.crypto import create_rsa_public_key
import jwt


class ResponseTypes(Enum):
    code = 0,


class AuthSchema(Enum):
    cookie = 0,
    header = 1


class OIDCConfig:

    def __init__(self, loop, issuer=None, client_id=None, client_secret=None,
                 redirect_uri=None, response_types=(ResponseTypes.code.name,),
                 scope=('openid', 'profile', 'email', 'offline_access'),
                 cookie_name='auth'
                 ):
        self.authority = issuer
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri
        self.scope = scope
        self.cookie_name = cookie_name
        self.response_types = response_types
        self.auth_endpoint_uri = self.create_auth_endpoint_uri()
        self.token_endpoint_uri = self.create_token_endpoint_uri()
        self.keys_endpoint_uri = self.create_keys_endpoint_uri()
        self.well_known_uri = self.create_well_known_uri()
        self.basic_auth = BasicAuth(self.client_id, self.client_secret, 'utf-8').encode()
        self.keys = {}
        self._loop = loop

    def create_auth_endpoint_uri(self):
        return '{0}?{1}'.format(urljoin(self.authority, 'auth'), self.create_auth_request_params())

    def create_token_endpoint_uri(self):
        return urljoin(self.authority, 'token')

    def create_keys_endpoint_uri(self):
        return urljoin(self.authority, 'keys')

    def create_well_known_uri(self):
        return urljoin(self.authority, '.well-known/openid-configuration')

    def create_token_request_params(self, code):
        return urlencode({'grant_type': 'authorization_code', 'code': code, 'redirect_uri': self.redirect_uri})

    def create_auth_request_params(self):
        return urlencode({'client_id': self.client_id, 'redirect_uri': self.redirect_uri,
                          'response_type': str.join(' ', self.response_types), 'scope': str.join(' ', self.scope)})

    async def validate_token(self, token):
        try:
            header = jwt.get_unverified_header(token)
            pub_key = await self.get_public_key(header.get('kid'))
            if pub_key is not None:
                payload = jwt.decode(token, key=pub_key, audience=self.client_id)
                return payload
            return None
        except:
            return None

    async def get_payload_by_code(self, code):
        try:
            async with ClientSession(loop=self._loop) as session:
                async with session.post(self.token_endpoint_uri,
                                        data=self.create_token_request_params(code),
                                        headers={'Authorization': self.basic_auth,
                                                 'Content-Type': 'application/x-www-form-urlencoded'}
                                        ) as resp:
                    jwt_token = await resp.json(loads=deserialize)
                    payload = await self.validate_token(jwt_token['id_token'])
                    return payload, jwt_token['id_token']
        except Exception as x:
            return None, None

    async def get_public_key(self, kid):
        key = self.keys.get(kid, None)
        if key is None:
            try:
                await self.init_keys()
                key = self.keys.get(kid, None)
            except:
                pass
        return key

    async def init_keys(self):
        async with ClientSession(loop=self._loop) as session:
            async with session.get(self.keys_endpoint_uri) as resp:
                keys = await resp.json(loads=deserialize)
                self.keys = {val['kid']: create_rsa_public_key(val['n'], val['e']) for val in keys.get('keys', [])}

    async def reinit_config_from_server(self):
        try:
            async with ClientSession(loop=self._loop) as session:
                async with session.get(self.well_known_uri) as resp:

                        config = await resp.json(loads=deserialize)
                        auth_endpoint = config.get('authorization_endpoint', None)
                        if auth_endpoint is not None:
                            self.auth_endpoint_uri = '{0}?{1}'.format(auth_endpoint, self.create_auth_request_params())
                        token_endpoint = config.get('token_endpoint', None)
                        if token_endpoint is not None:
                            self.token_endpoint_uri = token_endpoint
                        keys_endpoint = config.get('jwks_uri', None)
                        if keys_endpoint is not None:
                            self.keys_endpoint_uri = keys_endpoint
        except Exception as x:
            print(x)
