__author__ = 'dimitriy'

async def set_user(request, payload, storage):
    # st = storage(request.app.data_providers)
    request.user = payload.get('sub', None)  # await st.get(payload.get('sub', None))

async def on_prepare_tokenizing_response(request, response):
    if request.jwt_token:
        response.headers['authorization'] = 'Bearer ' + request.jwt_token
        response.set_cookie(request.app.auth_config.cookie_name, request.jwt_token)
    else:
        response.del_cookie(request.app.auth_config.cookie_name)


def oidc_middleware(storage):
    async def factory(app, handler):
        async def middleware(request):
            request.user = None
            request.jwt_token = None
            payload = None
            jwt_token = request.headers.get('authorization', None)
            if jwt_token is None:
                jwt_token = request.cookies.get(app.auth_config.cookie_name, None)
            else:
                parts = jwt_token.split()

                if parts[0].lower() == 'bearer' and len(parts) == 2:
                    jwt_token = parts[1]
            if jwt_token:
                    payload = await app.auth_config.validate_token(jwt_token)
            else:
                code = request.rel_url.query.get('code', None)
                if code is not None:
                    payload, jwt_token = await app.auth_config.get_payload_by_code(code)

            if payload is not None:
                await set_user(request, payload=payload, storage=storage)
                if request.user is not None:
                    request.jwt_token = jwt_token

            response = await handler(request)

            return response
        return middleware
    return factory
