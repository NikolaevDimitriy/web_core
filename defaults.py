import string

# root url configuration module name
ROOT_URLCONF = 'aio_urls'

USER_REQUEST_KEY = 'X-USER'
USER_SESSION_KEY = '_auth_user_id'
CSRF_TOKEN_NAME = 'csrf_token'
