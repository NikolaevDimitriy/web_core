__author__ = 'dimitriy'
from collections import namedtuple


class SIEResult(namedtuple('SIEResult', ['success', 'instance', 'errors'])):
    def to_dict(self, **kwargs):
        return {
            'success': self.success,
            'instance': self.instance.to_dict(**kwargs) if self.instance is not None else '',
            'errors': [x.to_dict() if x is ErrorResult else x for x in self.errors] if self.errors is not None else []
        }


class ErrorResult(namedtuple('ErrorResult', ['identity', 'errors'])):
    def to_dict(self, **kwargs):
        return {
            'identity': self.identity,
            'errors': [x.to_dict() if x is ErrorResult else x for x in self.errors] if self.errors is not None else []
        }