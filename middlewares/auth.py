__author__ = 'dimitriy'

from aiohttp_session import get_session, SESSION_KEY
from web_core.defaults import USER_SESSION_KEY



def get_auth_middleware(auth_backend):
    return auth_middleware(auth_backend)


def _get_user_session_key(request):
    return int(request[SESSION_KEY].get(USER_SESSION_KEY, 0))


def auth_middleware(auth_backend):
    async def factory(app, handler):
        async def middleware(request):
            session = await get_session(request)
            if session is not None:
                user_id = _get_user_session_key(request)
                user = await auth_backend.get_user(user_id)
                if user is not None:
                    request.user = user
                    session.changed()
            response = await handler(request)
            return response
        return middleware
    return factory