__author__ = 'dimitriy'

from web_core.defaults import USER_REQUEST_KEY


def get_xuser_middleware(auth_backend):
    return xuser_middleware(auth_backend)


def _get_user_id(request):
    return int(request.headers.get(USER_REQUEST_KEY, 0))


def xuser_middleware(auth_storage):
    async def factory(app, handler):
        async def middleware(request):
            user_id = int(request.headers.get(USER_REQUEST_KEY, 0))
            try:
                user = await auth_storage.get(user_id)
                if user is not None:
                    request.user = user
            except Exception as x:
                pass
            response = await handler(request)
            return response
        return middleware
    return factory