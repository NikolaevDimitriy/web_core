__author__ = 'dimitriy'
import uuid
from aiohttp.web import HTTPForbidden
from aiohttp_session import get_session
from web_core.defaults import CSRF_TOKEN_NAME


def csrf_middleware():
    async def factory(app, handler):
        async def middleware(request):
            session = await get_session(request)
            if request.method == "POST":
                token = session[CSRF_TOKEN_NAME, None]
                await request.post()
                if not token or token != request.POST[CSRF_TOKEN_NAME]:
                    return HTTPForbidden
            else:
                request[CSRF_TOKEN_NAME] = uuid.uuid4().hex

            response = await handler(request)
            session[CSRF_TOKEN_NAME] = request[CSRF_TOKEN_NAME]
            return response
        return middleware
    return factory
