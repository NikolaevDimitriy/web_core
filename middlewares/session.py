__author__ = 'dimitriy'

import ujson as json
import uuid
from aiohttp_session import Session, session_middleware, AbstractStorage


def get_session_middleware(pool, cookie_name, age):
    return session_middleware(CustomRedisStorage(pool, cookie_name=cookie_name, max_age=age))


class CustomRedisStorage(AbstractStorage):
    """Redis storage"""

    def __init__(self, redis_pool, *, cookie_name="AIOHTTP_SESSION",
                 domain=None, max_age=None, path='/',
                 secure=None, httponly=True,
                 encoder=json.dumps, decoder=json.loads):
        super().__init__(cookie_name=cookie_name, domain=domain,
                         max_age=max_age, path=path, secure=secure,
                         httponly=httponly)
        self._encoder = encoder
        self._decoder = decoder
        self._redis = redis_pool

    async def load_session(self, request):
        cookie = self.load_cookie(request)
        if cookie is None:
            return Session(None, data=None, new=True)
        else:
            async with self._redis.get() as conn:
                key = str(cookie)
                data = await conn.get(self.cookie_name + '_' + key)
                if data is None:
                    return Session(None, new=True)
                if not isinstance(data, str):
                    data = data.decode('utf-8')
                data = self._decoder(data)
                return Session(key, data=data, new=False)

    async def save_session(self, request, response, session):
        key = session.identity
        if key is None:
            key = uuid.uuid4().hex
            self.save_cookie(response, key)
        else:
            key = str(key)
            self.save_cookie(response, key)

        data = self._encoder(self._get_session_data(session))
        async with self._redis.get() as conn:
            max_age = self.max_age
            expire = max_age if max_age is not None else 0
            await conn.set(self.cookie_name + '_' + key, data, expire=expire)


